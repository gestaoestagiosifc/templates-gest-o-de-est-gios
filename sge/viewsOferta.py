#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
import datetime
import random


def emp_coord_consulta_candidatos(request):
	candidaturas =[]
	for n in [1, 2, 3, 4, 5]:
		aluno={'nome': 'Fernando da Silva '+str(n)}
		vaga ={'funcao': 'Função'+str(n)}
		candidatura={'aluno':aluno, 'vaga':vaga, 'data':'11/09/2014'}
		candidaturas.append(candidatura)

	return render_to_response('oferta/emp_coord_consulta_candidatos.html',{'candidaturas':candidaturas}, context_instance=RequestContext(request))

def emp_coord_detalhar_vaga(request):
	vaga = {'funcao':'Lorem ipsum dolor sit amet', 'descricao':'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet', 'turno':'Turno da vaga', 'hora_inicio':'00:00h', 'hora_fim':'00:00h', 'auxilios': {'auxilio1': '200,00', 'auxilio2': '50,00', 'auxilio3': '50,00'}, 'numero_vagas':'4'}
	return render_to_response('oferta/emp_coord_detalhe_vaga.html', {'vaga':vaga}, context_instance=RequestContext(request))


def emp_coord_add_oferta(request):

	inseridas=[]
	for n in [1, 2, 3, 4, 5]:
		dict = {'funcao': 'Função' + str(n),
                    'descricao': 'Descrição' + str(n), 'quantidade': 'Quantidade'+ str(n), 'turno':'Turno'+ str(n), 'hora_inicio':'12:00', 'hora_fim':'17:00'}
		inseridas.append(dict) 
	return render_to_response('oferta/emp_coord_add_oferta.html', {'inseridas':inseridas}, context_instance=RequestContext(request))

def emp_coord_add_auxilio(request):
	vaga = { 'funcao':'Função que será desempenhada', 'descricao':'Descrição detalhada da vaga que está sendo ofertada. Lorem Ipsum dolor sit amen.', 'turno':'Matutino'}
	auxilios=[]
	for n in [1, 2, 3, 4, 5]:
		x = random.randint(1, 9)
		if x%2 ==0:
			auxilio = 'vale transporte'
		else:
			auxilio = 'bolsa'
		dict = {'valor':'1.000,00', 'auxilio':'vale transporte'}
		auxilios.append(dict)
	return render_to_response('oferta/emp_coord_add_auxilio.html', { 'vaga':vaga, 'auxilios':auxilios},context_instance=RequestContext(request))


def emp_coord_consulta_oferta(request):
	todas = []
	preenchidas = []
	canceladas = []
	vencidas= []
	naopreenchidas = []
	for n in [1, 2, 3, 4, 5]:
		dict = {'titulo': 'Titulo' + str(n),
                    'data': '25/12/2012' , 'data_vencimento': '25/12/2018', 'totalVagas':n*3, 'vagasPreenchidas':n*2}
		naopreenchidas.append(dict)
		dict['status'] = 'status2'
		todas.append(dict)

		m = n+10
		dict = {'titulo': 'Titulo' + str(m),
                    'data': '25/12/2012' , 'data_vencimento': '25/12/2018', 'totalVagas':m*3, 'vagasPreenchidas':m*2}
		preenchidas.append(dict)
		dict['status'] = 'status1'
		todas.append(dict)

		o = n+7

		dict = {'titulo': 'Titulo' + str(o),
                    'data': '25/12/2012' , 'data_vencimento': '25/12/2018', 'totalVagas':o*3, 'vagasPreenchidas':o*2}
		canceladas.append(dict)
		dict['status'] = 'status3'
		todas.append(dict)

		p = o-2

		dict = {'titulo': 'Titulo' + str(p),
                    'data': '25/12/2012' , 'data_vencimento': '25/12/2018', 'totalVagas':p*3, 'vagasPreenchidas':p*2}
		vencidas.append(dict)
		dict['status'] = 'status4'
		todas.append(dict)

	return render_to_response('oferta/emp_coord_consulta_ofertas.html', {'todas':todas, 'naopreenchidas':naopreenchidas, 
		'preenchidas':preenchidas, 'canceladas':canceladas, 'vencidas':vencidas},context_instance=RequestContext(request))


def emp_coord_detalhe_candidato(request):
	curso={ 'nome':'Nome do Curso Sendo Cursado', 'tempo_minimo':'8' }
	aluno={ 'email':'email@email.com', 'first_name': 'Fernando da Silva x', 'curso':curso}

	candidato={'aluno':aluno}

	return render_to_response('oferta/emp_coord_detalhe_candidato.html',{'candidato':candidato}, context_instance=RequestContext(request))


def emp_coord_detalhar_oferta(request):

	vagas = []
	
	for n in [1, 2, 3]:
		dict = {'descricao': 'Descrição' + str(n), 'funcao': 'Função na empresa' + str(n), 'turno' : 'Vespertino', 'hora_inicio' : '00:00', 'hora_fim' : '00:00' }
		vagas.append(dict)
	
	total_vagas = len(vagas)

	oferta = {'descricao': 'Descrição', 'data_vencimento': '00/00/0000'}
	return render_to_response('oferta/emp_coord_detalhe_oferta.html', {'vagas':vagas, 'oferta':oferta,'total_vagas':total_vagas, 'vagas_disponiveis':4 },context_instance=RequestContext(request))


def emp_coord_consulta_vagas(request):

	vagas=[]

	for n in [2, 5, 5, 3, 2, 3, 5]:


		dict = {'status': 'status' + str(n), 'funcao': 'Função' + str(n), 'descricao': 'Descrição'+ str(n), 'turno':'Turno'+ str(n), 'auxilio':'Qtde auxilios' + str(n)}
		vagas.append(dict)

	return render_to_response('oferta/emp_coord_consulta_vagas.html', {'vagas':vagas}, context_instance=RequestContext(request))

def emp_coord_consulta_candidatos_por_vaga(request):
	candidatos = []
	vaga={'funcao':'funcao', 'turno':'Matutino', }
	for n in [1, 2, 3, 4, 5]:
		dict  = {'first_name':'Nome Pessoa'+str(n), 'data':'25/12/2002'}
		candidatos.append(dict)
	return render_to_response('oferta/emp_coord_consulta_candidato_vaga.html',{'vaga':vaga, 'candidatos':candidatos}, context_instance=RequestContext(request))

def coord_consulta_empresas(request):

	empresas=[]

	for n in [3, 5, 3, 5, 5, 3]:


		dict = {'status': 'status' + str(n), 'nome_empresa': 'Nome da empresa' + str(n), 'tipo_concedente': 'Tipo concedente'+ str(n), 'area_atuacao':'Área de atuação'+ str(n)}
		empresas.append(dict)


	return render_to_response('oferta/coord_consulta_empresas.html', {'empresas':empresas} , context_instance=RequestContext(request))


def aluno_listar_ofertas(request):
	
	ofertas = []

	for n in [1, 2, 3, 4, 5]:
		dict = {'concedente': 'Nome da empresa' + str(n),'descricao': 'Descrição' + str(n), 'data': '00/00/0000'}
		ofertas.append(dict)

	return render_to_response('oferta/aluno_listar_ofertas.html', {'ofertas': ofertas}, context_instance=RequestContext(request))

def aluno_minhas_vagas(request):

	vagas = []

	for n in [1, 2, 3, 4, 5]:
		dict = {'concedente': 'Nome da empresa' + str(n),
                    'descricao': 'Descrição' + str(n), 'funcao': 'Função na empresa' + str(n), 'status': 'status' + str(n) }
		vagas.append(dict)

	return render_to_response('oferta/aluno_minhas_vagas.html', {'vagas': vagas}, context_instance=RequestContext(request))


def aluno_detalhar_empresa(request):

	empresa = {'concedente': 'Nome da Empresa', 'funcao': 'Função na empresa', 'endereco': {'rua': 'Rua dos BOBOS', 'numero': '10', 'bairro': 'Bairro', 'cidade': 'Cidade', 'CEP': '89240-000', 'referencia': 'Referência'}, }
	oferta = {'descricao': 'Descrição', 'habilidades': 'Habilidades', 'area_atuacao': 'Área de Atuação', 'numero_vagas': '5', 'turno': '3 matutino, 2 vespertino', 'auxilios': [{'auxilio1': '200,00'}, {'auxilio2': '50,00'}, {'auxilio3': '50,00'}]}

	return render_to_response('oferta/aluno_detalhar_empresa.html', {'empresa':empresa}, context_instance=RequestContext(request))

def aluno_coord_detalhar_vaga(request):

	vaga = {'funcao':'Lorem ipsum dolor sit amet', 'descricao':'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet', 'turno':'Turno da vaga', 'hora_inicio':'00:00h', 'hora_fim':'00:00h', 'auxilios': {'auxilio1': '200,00', 'auxilio2': '50,00', 'auxilio3': '50,00'}, 'numero_vagas':'4'}
	return render_to_response('oferta/aluno_coord_detalhar_vaga.html', {'vaga':vaga}, context_instance=RequestContext(request))

def aluno_coord_detalhar_oferta(request):
	
	vagas = []
	
	for n in [1, 2, 3]:
		dict = {'descricao': 'Descrição' + str(n), 'funcao': 'Função na empresa' + str(n), 'turno' : 'Vespertino', 'hora_inicio' : '00:00', 'hora_fim' : '00:00' }
		vagas.append(dict)
	
	total_vagas = len(vagas)

	oferta 	= {'descricao': 'Descrição', 'data_vencimento': '00/00/0000'}
	empresa = {'concedente': 'TAG Interativa', 'area_atuacao': 'Tecnologia da Informação', 'website':'www.taginterativa.com.br', 'endereco': {'rua': 'Rua São Paulo', 'numero': '354, BL11 APT205', 'bairro': 'Bucarein', 'cidade': 'Joinville', 'CEP': '89240-000', 'referencia': 'Próximo ao restaurante Capim Teimoso '}, 'contato': 'Contatos da empresa'}

	return render_to_response('oferta/aluno_coord_detalhar_oferta.html', {'vagas':vagas, 'total_vagas':total_vagas, 'vagas_disponiveis':4,'oferta':oferta, 'empresa':empresa}, context_instance=RequestContext(request))

def aluno_escolhe_orientador(request):
	informacao_orientador = []

	for n in [1,2,3,4]:
		dict = {'nome': 'Nome do orientador' + str(n), 'curso': 'Nome do curso' + str(n), 'status': 'status' + str(n) }
		informacao_orientador.append(dict)

	return render_to_response('oferta/aluno_escolhe_orientador.html', {'informacao_orientador':informacao_orientador}, context_instance=RequestContext(request))

#def ori_consulta_solicitacao_orientacao(request):
#	return render_to_response('oferta/ori_consulta_orientacao.html', context_instance=RequestContext(request))

#def ori_detalha_solicitacao(request):
#	return render_to_response('oferta/ori_detalha_solicitacao.html', context_instance=RequestContext(request))

#def ori_consulta_alunos(request):
#	return render_to_response('oferta/ori_consulta_alunos.html', context_instance=RequestContext(request))

