#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
import datetime
import random


def criaEmpresa(i, j):
    solicita = datetime.date.today() - datetime.timedelta(days=(i * 30))
    modifica = datetime.date.today() - datetime.timedelta(days=(i * 10))
    n = str(i)
    x = random.randint(1, 9)
    if x % 2 == 0:
        contrato = 'Anexado'
    else:
        contrato = 'Não Anexado'
    dict = {'nome_empresa': 'Nome da empresa' + n, 'status': j, 'data_solicitacao':
            solicita.strftime("%d/%m/%Y"), 'data_modificacao': modifica.strftime("%d/%m/%Y"), 'contrato': contrato}
    return dict


def solicitacao(request):

    return render_to_response('convenio/solicitacao.html', context_instance=RequestContext(request))


def convenios_reitoria(request):

    # convênio por status

    empresas = []
    for i in [1, 2, 3, 4, 5]:
        solicita = datetime.date.today() - datetime.timedelta(days=(i * 30))
        modifica = datetime.date.today() - datetime.timedelta(days=(i * 10))
        n = str(i)
        x = random.randint(1, 9)
        if x % 2 == 0:
            contrato = 'Anexado'
        else:
            contrato = 'Não Anexado'
        dict = {'nome_empresa': 'Nome da empresa' + n, 'status': n, 'data_solicitacao':
                solicita.strftime("%d/%m/%Y"), 'data_modificacao': modifica.strftime("%d/%m/%Y"), 'contrato': contrato, 'x': x}
        empresas.append(dict)

    return render_to_response('convenio/convenios_reitoria.html', {'empresas': empresas}, context_instance=RequestContext(request))


def avaliacao_convenio(request):

    # avaliação de convênio por parte da reitoria (não permite edição)

    empresa = {'nome_empresa': 'Nome da Empresa', 'tipo_concedente': 'entidade_pliberal', 'cnpj': '', 'crea': '0123456789/SC', 'area_atuacao': 'Área de atuação',
               'first_name': 'Nome da pessoa', 'cpf': '012345678-90', 'email': 'email@empresa.com',
               'telefone': {'contato': '47 3333-3333'},
               'endereco': {'rua': 'Nome da Rua', 'numero': '123', 'bairro': 'Centro',
                            'cidade': 'Araquari', 'estado': 'Santa Catarina', 'cep': '12345-678', 'referencia': 'Referência para o endereço'}, 'username': 'login'}
    return render_to_response('convenio/avaliacao_convenio.html', {'empresa': empresa}, context_instance=RequestContext(request))


def meu_convenio(request):

    # convênio da empresa (permite edição)

    empresa = {'nome_empresa': 'Nome da Empresa', 'tipo_concedente': 'entidade_pliberal', 'cnpj': '',
               'crea': '0123456789/SC', 'area_atuacao': 'Área de atuação', 'first_name': 'Nome da pessoa',
               'cpf': '012345678-90', 'email': 'email@empresa.com', 'telefone': {'contato': '47 3333-3333'},
               'endereco': {'rua': 'Nome da Rua', 'numero': '123', 'bairro': 'Centro', 'cidade':
                            'Araquari', 'estado': 'Santa Catarina', 'cep': '12345-678', 'referencia': 'Referência para o endereço'}, 'username': 'login'}
    return render_to_response('convenio/meu_convenio.html', {'empresa': empresa}, context_instance=RequestContext(request))


def consulta_convenio(request):

    # consulta todos os convenios

    naoanalisados = []
    for j in [5, 7, 9, 11]:
        a = criaEmpresa(j, 1)
        naoanalisados.append(a)

    emAndamento = []
    for j in [2, 4, 6, 8, 10]:
        a = criaEmpresa(j, 2)
        emAndamento.append(a)

    aprovados = []
    for j in [1, 2, 3, 4, 5]:
        a = criaEmpresa(j, 3)
        aprovados.append(a)

    recusados = []
    for j in [25, 12, 15, 13, 7]:
        a = criaEmpresa(j, 4)
        recusados.append(a)

    cancelados = []
    for j in [17, 11, 6, 12, 8]:
        a = criaEmpresa(j, 5)
        cancelados.append(a)

    return render_to_response('convenio/convenios_status.html', {'naoanalisados': naoanalisados,
    	'emAndamento':emAndamento, 'aprovados':aprovados, 'recusados':recusados, 'cancelados':cancelados},
    	context_instance=RequestContext(request))

def login(request):
    return render_to_response('login.html', context_instance=RequestContext(request))

def home(request):
    return render_to_response('home.html', context_instance=RequestContext(request))
