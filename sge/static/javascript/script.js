$(document)

    .ready(function() {

        var numero_crea, uf_crea;

        id=$('article').attr('id')
        $('nav #'+id).addClass('active')

        $('.ui.dropdown')
          .dropdown()
        ;

        // Habilita o accordion

        $('.ui.accordion')
          .accordion()
        ;

        //Habilita a navegação nos steps

        $('.ui.labeled.icon.button')
          .tab()

          $('.filter.menu .step')
          .tab()
          ;

        ;

        $('.ui.radio.checkbox')
          .checkbox()
        ;

        //Habilita navegação dos tabs

        $('.filter.menu .item')
          .tab()
        ;

        // Habilita campo de cnpj ou crea de acordo com a escolha do select

        $('#tipo_concedente').change(function(){

          var tipo_concedente = $('#tipo_concedente option:selected').val();

          if (tipo_concedente == 1 || tipo_concedente == 2 ) {

            $('.cnpj').show();
            $('.crea').hide();

          }else{

            $('.crea').show();
            $('.cnpj').hide();

          };

        });


        // Pega o número do crea quando o foco sai do input 

        $("#numero_crea").blur(function() {

          uf_crea = $('#uf_crea option:selected').text();

          numero_crea = $("#numero_crea").val();
          
          var crea = numero_crea + "/" + uf_crea;

          $('#crea').val(crea);

        });


        // Pega a uf do crea com o campo select

        $('.reitoria #uf_crea').change(function(){

          uf_crea = $('#uf_crea option:selected').text();

          numero_crea = $("#numero_crea").val();

          var crea = numero_crea + "/" + uf_crea;

          $('#crea').val(crea);

        });


        // separando o crea para exibição    

        $('.reitoria').ready(function(){

          var crea = $('#crea').val();
          var crea_valor = crea.split('/');

          // numero do crea
          $('#numero_crea').val(crea_valor[0]);

          // uf do crea
          $('#uf_crea').val(crea_valor[1]);

        })

        // se a opção 'convênio valido' for selecionada, ocultar text área para observações

        $('#textarea').show();
        
        $('#acoes_contrato_valido').hide();

        $('.radio').click(function(){


            if ($('#valido').is(':checked')) {

                $('#textarea').hide();
                $('#acoes_contrato_valido').show();

            }else{

                $('#textarea').show();
                $('#acoes_contrato_valido').hide();

            };

        });

    });

;