#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from sge import views, viewsOferta

from django.contrib import admin
admin.autodiscover()

from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = patterns('',
    url(r'^media/(.*)$', 'django.views.static.serve',
    {'document_root': settings.MEDIA_ROOT}),    
    # Examples:
    # url(r'^$', 'sge.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    # urls convênio

    url(r'^solicitacao/', views.solicitacao, name='nSolicitacao'),
    url(r'^convenios_reitoria/', views.convenios_reitoria, name='nConvenios_reitoria'),
    url(r'^avaliacao_convenio/', views.avaliacao_convenio, name='nAvaliacao_convenio'),
    url(r'^meu_convenio/', views.meu_convenio, name='nMeu_convenio'),
    url(r'^consulta_convenio/', views.consulta_convenio, name='nConsulta_convenio'),

    # urls oferta

    url(r'^aluno_listar_ofertas/', viewsOferta.aluno_listar_ofertas, name='nAlunoListarOfertas'),
    url(r'^aluno_minhas_vagas/', viewsOferta.aluno_minhas_vagas, name='nMinhasVagas'),
    url(r'^aluno_coord_detalhe_oferta/', viewsOferta.aluno_coord_detalhar_oferta, name='nAlunoCoordDetalharOferta'),
    url(r'^aluno_coord_detalhe_vaga/', viewsOferta.aluno_coord_detalhar_vaga, name='nAlunoCoordDetalharVaga'),
    url(r'^aluno_escolhe_orientador/', viewsOferta.aluno_escolhe_orientador, name='nEscolherOrientador'),
    url(r'^coord_consulta_empresas/', viewsOferta.coord_consulta_empresas, name='nCoordConsultaEmpresas'),
    url(r'^emp_coord_add_ofertas/', viewsOferta.emp_coord_add_oferta, name='nAddOfertas'),
    url(r'^emp_coord_consulta_ofertas/', viewsOferta.emp_coord_consulta_oferta, name='nEmpCoordConsultaOferta'),
    url(r'^emp_coord_consulta_vagas/', viewsOferta.emp_coord_consulta_vagas, name='nEmpCoordConsultaVagas'),
    url(r'^emp_coord_add_auxilio/', viewsOferta.emp_coord_add_auxilio, name='nEmpCoordAddAuxilio'),
    url(r'^emp_coord_consulta_candidatos/', viewsOferta.emp_coord_consulta_candidatos, name='nEmpCoordConsultaCandidatos'),
    url(r'^emp_coord_detalhe_candidato/', viewsOferta.emp_coord_detalhe_candidato, name='nEmpCoordDetalharCandidato'),
    url(r'^emp_coord_detalhe_oferta/', viewsOferta.emp_coord_detalhar_oferta, name='nEmpCoordDetalharOferta'),
    url(r'^emp_coord_detalhe_vaga/', viewsOferta.emp_coord_detalhar_vaga, name='nEmpCoordDetalharVaga'),
    url(r'^emp_coord_consulta_candidatos_por_vaga/', viewsOferta.emp_coord_consulta_candidatos_por_vaga, name='nEmpCoordConsultaCandidatoPorVaga'),
    #url(r'^ori_consulta_orientacao/', viewsOferta.ori_consulta_solicitacao_orientacao, name='nOriConsultaOrientacao'),




    url(r'^login/', views.login, name='login'),
    url(r'^home/', views.home, name='home'),

)


urlpatterns += staticfiles_urlpatterns()
